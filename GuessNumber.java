package Example;

import java.util.Scanner;

public class GuessNumber {

	static boolean checkRangeofNumber(int guessTheNumber) {
		if ((guessTheNumber >= 1 && guessTheNumber <= 100)) {

			return true;
		} else
			return false;
	}

	public static void main(String[] args) {

		randomNumber = (int) (Math.random() * 99 + 1);
		boolean guessedNumber = false;
		System.out.println("guess a number between 1 to 100");
		Scanner scanner = new Scanner(System.in);
		int guessTheNumber = scanner.nextInt();
		int numberOfAttempts = 0;
		while (!guessedNumber) {
			if (!checkRangeofNumber(guessTheNumber)) {
				System.out.println("I won't count this number,guess again betweem 1 to 100");
				guessTheNumber = scanner.nextInt();
			} else {
				numberOfAttempts += 1;
			}

			if (guessTheNumber < randomNumber) {
				System.out.println("Too low, Guess again");
				guessTheNumber = scanner.nextInt();
			} else if (guessTheNumber > randomNumber) {
				System.out.println("Too high,guess again");
				guessTheNumber = scanner.nextInt();
			} else {
				System.out.println("you guessed it in " + numberOfAttempts + "attempts");
				guessedNumber = true;
			}
		}
	}

}
