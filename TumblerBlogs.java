package Example;
//one line space between name of package and libraries imported
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;
import org.json.JSONArray;
import org.json.JSONObject;
//one line space between libraries and start of class
public class TumblerBlogs {
//one variable declaration at one line(horizontal spacing)
	String inline = "";
	String output = "";
//no space between method name and paranthesis because both are closely associated
	void urlConnection(String blogName, int range[]) {

		try {
			// Establishing url connection and fetching data from tumblr.com
			URL url = new URL("https://" + blogName + ".tumblr.com/api/read/json?type=photo&num=" + range[1] + "&start="
					+ range[0]);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.connect();
			Scanner scanner = new Scanner(url.openStream());
			while (scanner.hasNext()) {
				inline += scanner.nextLine();
			}
			scanner.close();
			while (inline.startsWith("var tumblr_api_read = ")) {
				output = inline.substring("var tumblr_api_read = ".length());
				output = output.replace(";", "");
				break;
			}
			connection.disconnect();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
//vertical line space between methods
// one horizontal space between each key word
	public static void main(String[] args) {

		System.out.println("Enter The name of blog : ");
		Scanner scan = new Scanner(System.in);
		String blogName = scan.next();
		System.out.println("\nEnter The Range : ");
		// Taking minimum and maximum range to display number of posts.
		// if input is more than 50 ask user to input again
		int rangeOfPosts[] = new int[2];
		for (int i = 1; i < rangeOfPosts.length; i++) {
			rangeOfPosts[i] = scan.nextInt();
			if (rangeOfPosts[i] > 50) {
				System.out.println("invalid input enter less than 50");
				break;
			} else {
				continue;
			}
		}
		Blogs blogs = new Blogs();
		blogs.urlConnection(blogName, rangeOfPosts);
		try {
			JSONObject jsonObject = new JSONObject(blogs.output);
			JSONObject jsonObject1 = new JSONObject(jsonObject.getJSONObject("tumblelog").toString());
			System.out.println("\nTitle : " + jsonObject1.get("title").toString());
			System.out.println("Description : " + jsonObject1.get("description").toString());
			System.out.println("Name : " + jsonObject1.get("name").toString());
			System.out.println("Number Of Posts : " + jsonObject.get("posts-total").toString());
			// displaying all posts
			JSONArray jsonObject2 = new JSONArray(jsonObject.getJSONArray("posts").toString());
			for (int i = 0; i < jsonObject2.length(); i++) {
				System.out.println("\ntitle : " + jsonObject2.get(i));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
